# About this project

This is the **Hot English Magazine** android application. Idea to develop such project dates back to the methods I had used to learn english language, specially listening skills to native english. 

The project aims to hold an app which tries to learn english following methods of [the Effortless English by A. J. Hoge](http://effortlessenglishclub.com/), and the [Learn Hot English Magazine](http://www.learnhotenglish.com/) resources.

Material is extracted from [the Effortless English by A. J. Hoge](http://effortlessenglishclub.com/) issues.


# Parts

The solution consists of three projects:
* A Ruby on Rails web server, which provides weekly magazine issues: `cem-server.git`,
* ~~A Java project that generates compressed bundle of issues preview,~~
* And this project, which is the end-user android application: `CoolEnglishMagazine.git`,

